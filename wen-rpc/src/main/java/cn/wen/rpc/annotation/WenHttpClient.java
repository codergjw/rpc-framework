package cn.wen.rpc.annotation;

import java.lang.annotation.*;

// TYPE代表可以放在 类和接口上面
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WenHttpClient {
    // 必填 代表bean的名称
    String value();
}
