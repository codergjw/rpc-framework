package cn.wen.rpc.message;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class WenMessage {

    // rpc message type
    private byte messageType;
    // serialization type
    private byte codec;
    // compress type
    private byte compress;
    // request id
    private int requestId;
    // request data
    private Object data;

}
