package cn.wen.rpc.exception;

public class WenRpcException extends RuntimeException {


    public WenRpcException(String msg){
        super(msg);
    }

    public WenRpcException(String msg, Exception e){
        super(msg,e);
    }
}
