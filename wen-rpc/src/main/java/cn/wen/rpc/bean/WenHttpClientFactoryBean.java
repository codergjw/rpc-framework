package cn.wen.rpc.bean;

import cn.wen.rpc.proxy.WenHttpClientProxy;
import org.springframework.beans.factory.FactoryBean;

public class WenHttpClientFactoryBean<T> implements FactoryBean<T> {

    private Class<T> interfaceClass;

    @Override
    public T getObject() throws Exception {
        // 返回一个代理实现类
        return new WenHttpClientProxy().getProxy(interfaceClass);
    }

    // 类型是接口
    @Override
    public Class<?> getObjectType() {
        return interfaceClass;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    public Class<T> getInterfaceClass() {
        return interfaceClass;
    }

    public void setInterfaceClass(Class<T> interfaceClass) {
        this.interfaceClass = interfaceClass;
    }
}
