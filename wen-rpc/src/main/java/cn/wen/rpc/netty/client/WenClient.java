package cn.wen.rpc.netty.client;


import cn.wen.rpc.message.WenRequest;

public interface WenClient {

    /**
     * 发送请求，并接收数据
     * @param wenRequest
     * @return
     */
    Object sendRequest(WenRequest wenRequest);
}
