package cn.wen.rpc.netty.handler;


import cn.wen.rpc.server.WenServiceProvider;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class WenRpcThreadFactory implements ThreadFactory {

    private WenServiceProvider wenServiceProvider;

    private static final AtomicInteger poolNumber = new AtomicInteger(1);
    private final AtomicInteger threadNumber = new AtomicInteger(1);

    private final String namePrefix;

    private final ThreadGroup threadGroup;

    public WenRpcThreadFactory(WenServiceProvider wenServiceProvider) {
        this.wenServiceProvider = wenServiceProvider;
        SecurityManager securityManager = System.getSecurityManager();
        threadGroup = securityManager != null ? securityManager.getThreadGroup() :Thread.currentThread().getThreadGroup();
        namePrefix = "wen-rpc-" + poolNumber.getAndIncrement()+"-thread-";
    }

    // 创建的线程以“N-thread-M”命名，N是该工厂的序号，M是线程号
    public Thread newThread(Runnable runnable) {
        Thread t = new Thread(threadGroup, runnable,
                namePrefix + threadNumber.getAndIncrement(), 0);
        t.setDaemon(true);
        t.setPriority(Thread.NORM_PRIORITY);
        return t;
    }
}
