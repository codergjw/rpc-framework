package cn.wen.rpc.netty.client.handler;

import cn.wen.rpc.exception.WenRpcException;
import cn.wen.rpc.message.WenResponse;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

public class UnprocessedRequests {

    private static final Map<String, CompletableFuture<WenResponse<Object>>> UP = new ConcurrentHashMap<>();

    public void put(String requestId,CompletableFuture<WenResponse<Object>> resultFuture){
        UP.put(requestId,resultFuture);
    }

    public CompletableFuture<WenResponse<Object>> complete(WenResponse<Object> msResponse){
        CompletableFuture<WenResponse<Object>> completableFuture = UP.remove(msResponse.getRequestId());
        if (completableFuture != null){
            completableFuture.complete(msResponse);
            return completableFuture;
        }
        throw new WenRpcException("获取结果数据出现问题");
    }
}
