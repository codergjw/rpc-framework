package cn.wen.rpc.consumer.rpc;

import cn.wen.rpc.annotation.WenHttpClient;
import cn.wen.rpc.annotation.WenMapping;
import cn.wen.rpc.provider.service.model.Goods;

@WenHttpClient(value = "goodsHttpRpc")
public interface GoodsHttpRpc {

    //发起网络调用 调用provider 商品查询服务
    @WenMapping(url = "http://localhost:7777",api = "/provider/goods/{id}")
    Goods findGoods(Long id);
}
